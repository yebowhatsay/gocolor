package color

import "fmt"

//Terminal ANSI escape codes used to output ed text in console
var Terminal = struct {
	Reset string

	Red    string
	Green  string
	Yellow string
	Blue   string
	Purple string
	Cyan   string
	White  string
}{
	Reset: "\033[0m",

	Red:    "\033[31m",
	Green:  "\033[32m",
	Yellow: "\033[33m",
	Blue:   "\033[34m",
	Purple: "\033[35m",
	Cyan:   "\033[36m",
	White:  "\033[37m",
}

//T map of colors
var T = map[string]string{
	"Reset": "\033[0m",

	"Red":    "\033[31m",
	"Green":  "\033[32m",
	"Yellow": "\033[33m",
	"Blue":   "\033[34m",
	"Purple": "\033[35m",
	"Cyan":   "\033[36m",
	"White":  "\033[37m",
}

//TRed formats the input string with terminal color Red
func TRed(str string) string {
	return fmt.Sprintf("%s%s%s", Terminal.Red, str, Terminal.Reset)
}

//TGreen formats the input string with terminal color Green
func TGreen(str string) string {
	return fmt.Sprintf("%s%s%s", Terminal.Green, str, Terminal.Reset)
}

//TColor formats the input string with color as in param color
func TColor(color string, e interface{}) (formattedStr string) {
	switch e.(type) {
	case string:
		formattedStr = fmt.Sprintf("%s%s%s", T[color], e, Terminal.Reset)
	case int:
		formattedStr = fmt.Sprintf("%s%d%s", T[color], e, Terminal.Reset)
	}
	return
}
